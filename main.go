package main

import (
	"bufio"
	"os"
	"fmt"
	"strings"
	"strconv"
	"net/http"
	"sync"
	"time"
	"github.com/shirou/gopsutil/mem"
	"github.com/shirou/gopsutil/cpu"
)

type Configuration struct {
	interval float64
	timeout float64
	urls     []string
}

type Response struct {
	URL      string
	Response *http.Response
	Error    error
}

func main() {
	var config Configuration
	alreadyRunning := false
	canRun := true
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		text := scanner.Text()
		if strings.HasPrefix(text, "add url ") {
			url := strings.TrimPrefix(text, "add url ")
			if strings.HasPrefix(url, "http://") || strings.HasPrefix(url, "https://") {
				config.urls = append(config.urls, url)
			} else {
				fmt.Println("Protocol must be specified")
			}
		} else if strings.HasPrefix(text, "remove url ") {
			url := strings.TrimPrefix(text, "remove url ")
			if len(config.urls) == 0 {
				fmt.Println("Url list is empty")
			} else {
				found := false
				for i, e := range config.urls {
					if e == url {
						found = true
						config.urls = append(config.urls[:i], config.urls[i+1:]...)
					}
				}
				if !found {
					fmt.Println("Url not found")
				}
			}
		} else if strings.HasPrefix(text, "set interval ") {
			interval := strings.TrimPrefix(text, "set interval ")
			if intervalFloat, err := strconv.ParseFloat(interval, 64); err == nil {
				if intervalFloat == 0 {
					fmt.Println("Interval cannot be 0")
				} else {
					config.interval = intervalFloat
				}
			} else {
				fmt.Println("Invalid interval value")
			}
		} else if strings.HasPrefix(text, "set timeout ") {
			timeout := strings.TrimPrefix(text, "set timeout ")
			if timeoutFloat, err := strconv.ParseFloat(timeout, 64); err == nil {
				if timeoutFloat == 0 {
					fmt.Println("Timeout cannot be 0")
				} else {
					config.timeout = timeoutFloat
				}
			} else {
				fmt.Println("Invalid timeout value")
			}
		} else if text == "urls" {
			if len(config.urls) == 0 {
				fmt.Println("Url list is empty")
			} else {
				for _, element := range config.urls {
					fmt.Println(element)
				}
			}
		} else if text == "timeout" {
			if config.timeout == 0 {
				fmt.Println("Timeout is not set")
			} else {
				fmt.Println(config.timeout)
			}
		} else if text == "interval" {
			if config.interval == 0 {
				fmt.Println("Interval is not set")
			} else {
				fmt.Println(config.interval)
			}
		} else if text == "start" {
			canRun = true
			go func() {
				if config.interval == 0 {
					fmt.Println("Interval is not set")
					return
				} else if len(config.urls) == 0 {
					fmt.Println("Url list is empty")
					return
				} else if config.timeout == 0 {
					fmt.Println("Timeout is not set")
					return
				} else if alreadyRunning {
					fmt.Println("Already running")
					return
				}
				for canRun {
					alreadyRunning = true
					responses := [] Response{}
					var wg sync.WaitGroup
					for _, url := range config.urls {
						wg.Add(1)
						go func(url string) {
							defer wg.Done()
							client := http.Client{Timeout: time.Duration(config.timeout) * time.Second}
							resp, err := client.Get(url)
							responses = append(responses, Response{url, resp, err})
						}(url)
					}
					wg.Wait()
					if canRun {
						fmt.Println("*CHECKS*")
						for _, check := range responses {
							if check.Error == nil {
								if check.Response.StatusCode == 200 {
									fmt.Println(check.URL, "UP")
								} else {
									fmt.Println(check.URL, "DOWN")
								}
							} else {
								fmt.Println(check.URL, "DOWN")
							}
						}
						virtualMem, _ := mem.VirtualMemory()
						timeStats, _ := cpu.Times(false)
						fmt.Println("*MEMORY*")
						fmt.Println("Total", virtualMem.Total)
						fmt.Println("Free", virtualMem.Free)
						fmt.Println("UsedPercent", virtualMem.UsedPercent)
						fmt.Println("*CPU*")
						for _, timeStat := range timeStats {
							fmt.Println("User", timeStat.User)
							fmt.Println("System", timeStat.System)
							fmt.Println("Idle", timeStat.Idle)
							fmt.Println("Nice", timeStat.Nice)
							fmt.Println("Iowait", timeStat.Iowait)
							fmt.Println("Irq", timeStat.Irq)
							fmt.Println("Softirq", timeStat.Softirq)
						}
					}
					alreadyRunning = false
					time.Sleep(time.Second * time.Duration(config.interval))
				}
			}()
		} else if text == "stop" {
			canRun = false
		} else if text == "quit" {
			return
		} else {
			fmt.Println("Unknown command")
		}
	}
}
