# ABOUT #

Un exercitiu bun si interesant. Din pacate sau din fericire, nu m-am putut abtine si am facut urmatoarele modificari.
Orice url poate fi adaugat sau sters in timp ce monitorizarea este deja pornita. Optional se poate opri monitorizarea folosind cuvand cheie stop. Iesirea din program este posibila folosind cuvantul cheie quit. 
Url-urile pot fi afisate folosind comanda urls. Am adaugat interval si timeout.
Pentru sincronizarea gorutinelor am folosit un waitgroup intrucat mi s-a parut mult mai ok decat varianta cu select si label. 
Pentru afisarea informatiilor despre memorie si procesor am folosit pachetul gopsutil care este crossplatform.
Pentru url am evitat folosirea unei expresii regulare deoarece am incercat mai multe si toate aveau probleme la parsare (unele nu recunosteau protocolul, altele portul etc)
Ce nu imi place este faptul ca m-am ales cu o functie main gigantica si trebuie regandita, impartita in mai multe functii, am sa vad daca se poate face function composition pe undeva si partea de http ar trebui mutata intr-un fisier separat. In mare parte mi s-a parut un exericiu destul de banal, dar nici nu am avut timp decat cateva ore


# COMMANDS #						

1.add url { url } 			
2.remove url { url } 			
3.urls 						
4.set timeout { seconds }							
5.set interval { seconds } 				
6.stop 						
7.start						
8.timeout	
9.interval		
10.quit					

# DESCRIPTION #						
1.url must start with protocol name 					
2.self explaining				
3.show all urls					
4.GET Request timeout in seconds , float64							
5.time between get requests in seconds , float64						
6.stop monitoring						
7.start monitoring					
8.returns timeout value				
9.returns interval value				
10.terminate program				


# PARERI #	
Pareri ?